const functions = require("../utils/functions");

test('Test Add function', () => {
    expect(functions.add(2, 3)).toBe(5)
});

test('Test sub function', () => {
    expect(functions.sub(3, 5)).toBe(-2)
});

test('to Equal Demo', () => {
    var data = {
        name: 'Ahmed'
    }
    data['country'] = 'tunisia';
    expect(data).toEqual({
        name: 'Ahmed',
        country: 'tunisia'
    });
});

test('truth of null', () => {
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).toBeFalsy();
});


test('truth of zeor', () => {
    const n = 0;
    expect(n).not.toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
});

test('test comparison', () => {
    const value = 4 + 0.2;
    expect(value).toBeGreaterThan(3);
    expect(value).toBeGreaterThanOrEqual(3.5);
    expect(value).toBeLessThan(5);
    expect(value).toBeLessThanOrEqual(4.5);
    expect(value).toBeCloseTo(4.2);
});

test('string matcher', () => {
    expect('ahmed is a developer').toMatch(/ahm/);
});

const countries = ["India", "Tunisia", "South Africa", "France"];


test('matcher for iterables', () => {
    expect(countries).toContain('Tunisia');
    expect(new Set(countries)).toContain('France');
});



test('Exception Matcher', () => {
    expect(functions.invalidOperation).toThrow(Error);
    expect(functions.invalidOperation).toThrow('operation not allowed');
    expect(functions.invalidOperation).toThrow(/not allowed/);
}); 