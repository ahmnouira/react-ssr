import React, { Component } from "react";
import axios from "axios";
import "../public/style.css";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      error: props.error,
    };
  }

  static async getInitialProps() {
    try {
      const res = await axios.get("https://api.github.com/users");
      return {
        data: res.data,
      };
    } catch (e) {
      return {
        error: e,
      };
    }
  }

  // support for the experimental syntax 'classProperties' isn't currently enabeled
  // @babel/plugin-proposal-class-properties
  getUser = async () => {
    try {
      console.log("value", document.getElementById("inputTextBox").value);
      const res = await axios.get(
        "https://api.github.com/users/" +
          document.getElementById("inputTextBox").value
      );
      this.setState({
        data: [res.data],
        error: null,
      });
    } catch (error) {
      this.setState({
        data: null,
        error: error,
      });
    }
  };

  render() {
    const { error, data } = this.state;
    if (error) {
      return (
        <div className="container">
          <h1>Github Users</h1>
          <br />
          <form>
            <div className="form-group">
              <label>Enter username: </label>
              <input className="form-control" id="inputTextBox" type="text" />
            </div>
            <button
              className="btn btn-primary"
              type="button"
              onClick={this.getUser}
            >
              Get User
            </button>
          </form>
          <br />
          <p className="error">Error: {error.message}</p>
        </div>
      );
    } else {
      return (
        <div className="container">
          <h1>Github Users</h1>
          <br />
          <form>
            <div className="form-group">
              <label>Enter username: </label>
              <input className="form-control" id="inputTextBox" type="text" />
            </div>
            <button
              className="btn btn-primary"
              type="button"
              onClick={this.getUser}
            >
              Get User
            </button>
          </form>
          <br />
          <div className="container bg-light">
            {data &&
              data.map((item, index) => (
                <div key={index} className="UserBlock">
                  <img src={item.avatar_url} alt="User icon" />
                  <div className="UserDetails">
                    <p>Username: {item.login}</p>
                    <p>ID: {item.id}</p>
                  </div>
                </div>
              ))}
          </div>
        </div>
      );
    }
  }
}
