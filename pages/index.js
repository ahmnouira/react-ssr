import React, { Component } from "react";
import Timer from "../components/Timer";
import "../public/style.css";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";
import List from "../components/List";

const ITEMS = ["one", "two", "three"];

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
    };
  }

  tick() {
    this.setState(() => {
      return {
        time: new Date().toLocaleTimeString(),
      };
    });
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return (
      <div>
        <div className="jumbotron text-center">
          <p style={{ color: "red" }}>{this.state.time}</p>
          <Timer />
          <Link href="/GithubUsers" as="github-users">
            <a>Github Users</a>
          </Link>
          <List items={ITEMS} />
        </div>
      </div>
    );
  }
}
