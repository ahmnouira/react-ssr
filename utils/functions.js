const functions = {
    add: (n1, n2) => n1 + n2,
    sub: (n1, n2) => n1 - n2,
    invalidOperation: () => {
        throw new Error('operation not allowed');
    }
}

module.exports = functions; 