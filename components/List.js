import React from 'react';

export default function List(props) {


    const { items } = props;
    if (!items.length) {
        return (
            <span className="empty-message">
                No items in list
            </span>
        )
    }
    return (
        <ul className="list-items">
            {
                items && items.map(item => {
                    return (
                        <li key={item} className="item">
                            {item}
                        </li>
                    )
                })
            }
        </ul>
    )
}